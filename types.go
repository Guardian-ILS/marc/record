package marcxml

type Collection []Record

type Record struct {
	Leader        string
	ControlFields []ControlField
	DataFields    []DataField
}

type ControlField struct {
	Tag     string
	Content string
}

type DataField struct {
	Tag  string
	Ind1 byte
	Ind2 byte

	SubFields []SubField
}

type SubField struct {
	Code    byte
	Content string
}
